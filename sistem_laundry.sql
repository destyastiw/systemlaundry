-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 13, 2022 at 05:55 AM
-- Server version: 10.4.24-MariaDB
-- PHP Version: 7.4.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sistem_laundry`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `name`, `image`, `email`, `password`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'logo', 'admin@gmail.com', '$2y$10$MApB0Y2sywU3QpD9D1y0/uN6RWATa1.00E3fETdMqRC3lO/8jDzGK', '2022-07-12 19:19:16', '2022-07-12 19:19:16');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(36, '2014_10_12_000000_create_users_table', 1),
(37, '2014_10_12_100000_create_password_resets_table', 1),
(38, '2019_08_19_000000_create_failed_jobs_table', 1),
(39, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(40, '2022_06_10_080750_create_admin_table', 1),
(41, '2022_06_10_080831_create_package_table', 1),
(42, '2022_06_10_085007_create_orders_laundry_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders_laundry`
--

CREATE TABLE `orders_laundry` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `code_order` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `package_id` bigint(20) UNSIGNED NOT NULL,
  `total_price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_drop_laundry` date DEFAULT NULL,
  `date_take_laundry` date DEFAULT NULL,
  `date_finish_laundry` date DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders_laundry`
--

INSERT INTO `orders_laundry` (`id`, `code_order`, `package_id`, `total_price`, `user_name`, `user_phone`, `user_address`, `date_drop_laundry`, `date_take_laundry`, `date_finish_laundry`, `status`, `created_at`, `updated_at`) VALUES
(1, 'EX1', 1, '20000', 'Huang Renjun', '085267879054', 'Jilin,Tiongkok', '2022-06-13', '2022-06-13', '2022-06-14', 'finish', '2022-07-12 19:19:16', '2022-07-12 19:19:16'),
(2, 'EX2', 6, '35000', 'Bintang', '087648762453', 'Jalan Mangga no 25', NULL, NULL, NULL, 'Take', '2022-07-12 19:27:54', NULL),
(3, 'EX3', 7, '100000', 'Mark', '0867998567', 'jalan jambu no 9', NULL, NULL, NULL, 'Drop', '2022-07-12 19:59:13', NULL),
(4, 'EX4', 7, '50000', 'Jasmine', '08976344578', 'jalan jasmine no 04', NULL, NULL, NULL, 'Drop', '2022-07-12 20:05:42', NULL),
(5, 'EX5', 6, '30000', 'Justin', '08765889964', 'jalan jeruk no 09', NULL, NULL, NULL, 'Finish', '2022-07-12 20:21:03', NULL),
(6, 'EX6', 7, '50000', 'matahari', '0845683456', 'jalan matahari no 03', NULL, NULL, NULL, 'Drop', '2022-07-12 20:23:29', NULL),
(7, 'EX7', 2, '20000', 'Jesi', '08678777000', 'jalan jesi no 07', NULL, NULL, NULL, 'Take', '2022-07-12 20:25:11', NULL),
(8, 'LD8', 7, '5000', 'Kamila', '089675452534', 'Jalan Manggis no 05', '2022-07-10', NULL, NULL, 'Drop', '2022-07-12 20:40:43', NULL),
(9, 'LD9', 5, '30000', 'Budi', '08458761068', 'Jalan Kedodong no 13', '2022-07-12', NULL, NULL, 'Finish', '2022-07-12 20:43:20', NULL),
(10, 'LD10', 8, '10000', 'Mikaila', '089456731432', 'Jalan Markisa no 08', '2022-07-11', NULL, NULL, 'Take', '2022-07-12 20:45:07', NULL),
(11, 'LD11', 6, '30000', 'Mentari', '085412347865', 'Jalan Kiwi no 12', '2022-07-09', NULL, NULL, 'Drop', '2022-07-12 20:46:10', NULL),
(12, 'LD12', 7, '5000', 'Haikal', '086512436907', 'Jalan Kiwi no 04', '2022-07-08', NULL, NULL, 'Take', '2022-07-12 20:47:54', NULL),
(13, 'EX13', 1, '35000', 'Bunga', '081365432786', 'Jalan Mangga no 13', '2022-07-13', NULL, NULL, 'Drop', '2022-07-12 20:50:57', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package`
--

CREATE TABLE `package` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `package`
--

INSERT INTO `package` (`id`, `type`, `name`, `price`, `created_at`, `updated_at`) VALUES
(1, 'Express', 'Cuci Setrika', '35000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(2, 'Express', 'Cuci Kering', '50000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(3, 'Express', 'Dry Cleaning', '25000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(4, 'Express', 'Setrika', '20000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(5, 'Laundry', 'Cuci Setrika', '30000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(6, 'Laundry', 'Cuci Kering', '30000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(7, 'Laundry', 'Dry Cleaning', '5000', '2022-07-12 17:00:00', '2022-07-12 17:00:00'),
(8, 'Laundry', 'Setrika', '10000', '2022-07-12 17:00:00', '2022-07-12 17:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jaemin', 'jaemin@gmail.com', NULL, '$2y$10$uqNxIhDxv2yoAxd6FOoKEu.iOAEnf27hUmi/0LHCDzqpSiKEDzruW', 'h9QrbUccuP', '2022-07-12 19:19:16', '2022-07-12 19:19:16'),
(2, 'Jeno', 'jeno@gmail.com', NULL, '$2y$10$WvK3jwClLnkVAUbtfpzzNOkIME5igx9liEwcSgzsuyLMxbOsSdKgm', 'LUFDFkCjdM', '2022-07-12 19:19:16', '2022-07-12 19:19:16');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_laundry`
--
ALTER TABLE `orders_laundry`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_laundry_package_id_foreign` (`package_id`);

--
-- Indexes for table `package`
--
ALTER TABLE `package`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `orders_laundry`
--
ALTER TABLE `orders_laundry`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `package`
--
ALTER TABLE `package`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `orders_laundry`
--
ALTER TABLE `orders_laundry`
  ADD CONSTRAINT `orders_laundry_package_id_foreign` FOREIGN KEY (`package_id`) REFERENCES `package` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
