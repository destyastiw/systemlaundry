  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">

      <!-- Brand Logo -->
      <a href="index3.html" class="brand-link">
          <img src="{{ asset('assets/AdminLTE/dist/img/logolaundry.jpg') }}" alt="AdminLTE Logo"
              class="brand-image img-circle elevation-3" style="opacity: .8">
          <span class="brand-text font-weight-light">Blossom Laundry</span>
      </a>

      <!-- Sidebar -->
      <div class="sidebar">
          <!-- Sidebar user (optional) -->
          <div class="user-panel mt-3 pb-3 mb-3 d-flex">
              <div class="image">
                  <img src="{{ asset('assets/AdminLTE/dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2"
                      alt="User Image">
              </div>
              <div class="info">
                  <a href="#" class="d-block">Admin</a>
              </div>
          </div>


          <!-- Sidebar Menu -->
          <nav class="mt-2">
              <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                  data-accordion="false">
                  <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
                  <li class="nav-item">

                      {{-- @if (auth()->user()->level == 1) --}}
                      <a href="{{ url('admin/dashboard/home') }}"
                          class="nav-link {{ Request::is('admin/dashboard/home') ? 'active' : '' }} ">
                          <i class="fas fa-tachometer-alt"></i>
                          <p>
                              Dashboard
                          </p>
                      </a>
                  </li>
                  {{-- {{ set_active('backend.dashboard.index') }} --}}
                  <li class="nav-item">
                      <a href="{{ url('admin/index') }}"
                          class="nav-link {{ Request::is('admin/index') ? 'active' : '' }}">
                          <i class="fa fa-user"></i>
                          <p>
                              Admin
                          </p>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a href="{{ url('admin/package/index') }}"
                          class="nav-link {{ Request::is('admin/package/index') ? 'active' : '' }}">
                          <i class="fa fa-box"></i>
                          <p>
                              Package
                          </p>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a href="{{ url('admin/listorder/index') }}"
                          class="nav-link {{ Request::is('admin/listorder/index') ? 'active' : '' }}">
                          <i class="fa fa-list"></i>
                          <p>
                              List Order
                          </p>
                      </a>
                  </li>

                  <li class="nav-item">
                      <a href="{{ url('admin/laporan/index') }}"
                          class="nav-link {{ Request::is('admin/laporan/index') ? 'active' : '' }}">
                          <i class="fa fa-file-excel"></i>
                          <p>
                              Laporan
                          </p>
                      </a>
                  </li>
                  {{-- @endif --}}
                  <li class="nav-item">
                      <a href="{{ url('frontend') }}"
                          class="nav-link {{ Request::is('frontend') ? 'active' : '' }}">
                          <i class="fa fa-angle-left"></i>
                          <p>
                              Halaman Depan
                          </p>
                      </a>
                  </li>



              </ul>
          </nav>
          <!-- /.sidebar-menu -->
      </div>
      <!-- /.sidebar -->
  </aside>
