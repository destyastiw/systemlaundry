<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Blossom Laundry</title>

    <!-- font awesome cdn link  -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">

    <!-- custom css file link  -->
    <link rel="stylesheet" href="{{ url('style.css') }}">
    <link rel='stylesheet' href='https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.min.css'>

</head>

<body>

    <!-- header section starts  -->

    <header>

        <a href="#" class="logo"><span>Blossom</span>Laundry</a>

        <input type="checkbox" id="menu-bar">
        <label for="menu-bar" class="fas fa-bars"></label>

        <nav class="navbar">
            <a href="#home">home</a>
            <a href="#service">service</a>
            <a href="#about">about</a>
            <a href="#review">review</a>
            <a href="#order">order</a>
            <a href="#ambilpaket">ambil laundry</a>
        </nav>

    </header>

    <!-- header section ends -->

    <!-- home section starts  -->

    <section class="home" id="home">

        <div class="content">
            <h3>Solution For Your<span> Dirty Clothes</span></h3>
            <p>Blossom Laundry is a website that will connect you with the best laundry services around you</p>
            <a href="#order" class="btn">Order</a>
        </div>

        <div class="image">
            <img src="images/home1.png" alt="">
        </div>

    </section>

    <!-- home section ends -->

    <!-- service section starts  -->

    <section class="service" id="service">

        <h1 class="heading"> Service </h1>

        <div class="box-container">

            <div class="box">
                <img src="images/service1.png" alt="">
                <h3>Cuci Premium</h3>
                <p>Pakaian dicuci dengan sistem 1 nota 1 mesin menggunakan chemical premium dari deterjen, sour hingga
                    softener.</p>
            </div>

            <div class="box">
                <img src="images/service2.png" alt="">
                <h3>Setrika</h3>
                <p>Pakaian kami setrika uap, sehingga terjamin akan kerapiannya serta pakaian tidak mudah kusut</p>
            </div>

            <div class="box">
                <img src="images/service3.png" alt="">
                <h3>Lipat</h3>
                <p>lipat secara rapi.Untuk Cuci Kering Lipat, setrika hanya bagian tertentu seperti kerah kemeja</p>
            </div>

        </div>

    </section>

    <!-- service section ends -->

    <!-- about section starts  -->

    <section class="about" id="about">

        <h1 class="heading"> Who We Are? </h1>

        <div class="column">

            <div class="image">
                <img src="images/about1.png" alt="">
            </div>

            <div class="content">
                <h3>Blossom Laundry</h3>
                <p>Layanan laundry handal dan berkualitas yang sudah dibuktikan dengan pelayanan terhadap pelanggannya
                </p>
                <p>Kami menyediakan dengan layanan Laundry Express, cucian anda akan selesai maksimal dalam 1 hari
                    saja dan menyediakan dry cleaning</p>
            </div>
        </div>

        </div>

    </section>

    <!-- about section ends -->

    <!-- newsletter  -->

    <div class="newsletter">

        <h3>Laundry Yang Bikin Tenang</h3>
        <p>Pakaian Anda dijamin bersih, rapi & wangi. Anda juga tidak perlu takut pakaian dicampur, hilang atau rusak
            dengan sistem 1 nota 1 mesin.</p>
    </div>

    <!-- review section starts  -->

    <section class="review" id="review">

        <h1 class="heading"> review pelanggan </h1>

        <div class="box-container">

            <div class="box">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic1.png" alt="">
                    <h3>Abigail Putri</h3>
                    <div class="stars">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                    <div class="comment">
                        saya laundry kemeja kantor disini, diambil dan diantar ,hasilnya juga memuaskan wangi dan
                        bersih, terima kasih.
                    </div>
                </div>
            </div>

            <div class="box">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic2.png" alt="">
                    <h3>Gloria Jesicca</h3>
                    <div class="stars">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star-half-alt"></i>
                    </div>
                    <div class="comment">
                        pertama kali pakai jasa laundry ini, pelayanannya sangat ramah, layanan yang memuaskan
                        ..recommended pokoknya..
                    </div>
                </div>
            </div>

            <div class="box">
                <i class="fas fa-quote-right"></i>
                <div class="user">
                    <img src="images/pic3.png" alt="">
                    <h3>Thalita Permata</h3>
                    <div class="stars">
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                        <i class="fas fa-star"></i>
                    </div>
                    <div class="comment">
                        Wangi dan rapi walaupun hanya cuci kering saja.Cara melipat bajunya rapi yang
                        paling aku suka adalah pelayanannya yang sangat cepat..
                    </div>
                </div>
            </div>

        </div>

    </section>

    <!-- review section ends -->

    <!-- order section starts  -->

    <section class="order" id="order">

        <h1 class="heading"> Order </h1>

        <div class="box-container">

            <div class="box">
                <h3 class="title">Blossom Laundry</h3>
                <div class="type">Express</div>
                <ul>
                    <li> Cuci Setrika </li>
                    <li> Cuci Kering </li>
                    <li> Setrika </li>
                    <li> Dry Cleaning </li>
                </ul>
                <a href="frontend/express" class="btn">order</a>
            </div>

            <div class="box">
                <h3 class="title">Blossom Laundry</h3>
                <div class="type">Laundry</div>
                <ul>
                    <li> Cuci Setrika </li>
                    <li> Cuci Kering </li>
                    <li> Setrika </li>
                    <li> Dry Cleaning </li>
                </ul>
                <a href="frontend/laundry" class="btn">order</a>
            </div>

        </div>

    </section>

    <!-- order section ends -->

    <!-- ambilpaket section starts  -->

    <section class="ambilpaket">

        <div class="image">
            <img src="images/printnota.png" alt="">
        </div>

        <form action="">

            <h1 class="heading">Ambil Laundry</h1>

            <div class="inputBox">
                <input type="text" name="code_order" required>
                <label>Code Order</label>
            </div>

            <button type="submit" class="btn" id="ambilpaket">Ambil Laundry</button>

        </form>

    </section>

    <!-- ambilpaket section edns -->


    <!-- footer section starts  -->

    <div class="footer">

        <div class="box-container">

            <div class="box">
                <h3>about us</h3>
                <p>Blossom Laundry adalah Layanan laundry handal dan berkualitas yang sudah dibuktikan dengan pelayanan
                    terhadap pelanggannya</p>
            </div>

            <div class="box">
                <h3>quick links</h3>
                <a href="#">home</a>
                <a href="#">service</a>
                <a href="#">about</a>
                <a href="#">review</a>
                <a href="#">order</a>
                <a href="#">ambil laundry</a>
            </div>

            <div class="box">
                <h3>follow us</h3>
                <a href="#">instagram</a>
                <a href="#">tik tok</a>
                <a href="#">twitter</a>
            </div>

            <div class="box">
                <h3>contact info</h3>
                <div class="info">
                    <i class="fas fa-phone"></i>
                    <p> 0853-8735-2678 <br> 0567-2435-7863 </p>
                </div>
                <div class="info">
                    <i class="fas fa-envelope"></i>
                    <p> blossom@gmail.com <br> laundry@gmail.com </p>
                </div>
                <div class="info">
                    <i class="fas fa-map-marker-alt"></i>
                    <p> Semarang, Indonesia 1998 </p>
                </div>
            </div>

        </div>

        <h1 class="credit"> &copy; copyright @ 2022 by Blossom Laundry </h1>

    </div>

    <!-- footer section ends -->

    <!-- JS sweet alert -->
    <script src="https://code.jquery.com/jquery-3.6.0.slim.js"
        integrity="sha256-HwWONEZrpuoh951cQD1ov2HUK5zA5DwJ1DNUXaM6FsY=" crossorigin="anonymous"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    <!-- JS sweet alert end -->
    <script>
        $('#ambilpaket').click(function() {
            swal({
                    title: "Yakin ?",
                    text: "apakah anda yakin ingin mengambil paket laundry ?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                })
                .then((willDelete) => {
                    if (willDelete) {
                        window.location = "/frontend/ambilpaket"
                        swal("paket laundry berhasil diambil!", {
                            icon: "success",
                        });
                    } else {
                        swal("paket laundry tidak jadi diambil!");
                    }
                });
        });
    </script>
</body>


</html>
