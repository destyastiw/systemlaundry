@extends('layouts.backend_dashboard.app')

@section('content')

    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Admin Detail</h3>
                        </div>
                        <br />
                        <div class="container">
                            <div class="row justify-content-center">
                                <table class="card-body">
                                    <tr>
                                        <th width="100px">Name</th>
                                        <th width="30px">:</th>
                                        <th>{{ $user->name }}</th>
                                    </tr>
                                    <br>
                                    <tr>
                                        <th width="100px">Email</th>
                                        <th width="30px">:</th>
                                        <th>{{ $user->email }}</th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <br>
                                            <a href="{{ url('admin/index') }}"
                                                type="submit"class="btn btn-primary">Back</a>
                                            <br>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endsection
