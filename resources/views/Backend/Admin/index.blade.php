@extends('layouts.backend_dashboard.app')
@section('Tampilan Crud')

@section('content')

    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>Admin</title>
    </head>

    <body>

        <div class="container">
            <div class="container">
                <a href="{{ url('admin/formadd') }}" type="button" class="btn btn-warning">Add</a><br>
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-auto">
                    </div>
                </div>
                <form method="GET" action="{{ url('admin/index') }}">
                    <input type="text" name="keyword" />
                    <button class="btn-xs-flat btn-success" type="submit">Search</button>
                </form>
                <br />
                <div class="row">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">Id</th>
                                <th scope="col">Name</th>
                                <th scope="col">Email</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php $id = 1; ?>
                            @foreach ($errors as $index => $data)
                                <tr>
                                    <th scope="row">{{ $index + $errors->firstItem() }}</th>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->email }}</td>
                                    <td>
                                        <a href="{{ url('/admin/detailadmin/' . $data->id) }}" class="btn-sm btn-primary">
                                            <i class="fas fa-folder"> </i> View</a>
                                        <a href="{{ url('/admin/formedit/' . $data->id) }}" class="btn-sm btn-info">
                                            <i class="fas fa-pencil-alt"> </i> Edit</a>
                                        <a href="{{ url('/admin/delete/' . $data->id) }}" class="btn-sm btn-danger">
                                            <i class="fas fa-trash"> </i> Delete</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    {{ $errors->links() }}
                </div>
            </div>
        </div>

        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
        </script>

    </body>

    </html>

@endsection
