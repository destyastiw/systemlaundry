@extends('layouts.backend_dashboard.app')

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Package Detail</h3>
                    </div>
                    <br />
                    <div class="container">
                        <div class="row justify-content-center">
                            <table class="card-body">
                                <tr>
                                    <th width="100px">Type</th>
                                    <th width="30px">:</th>
                                    <th>{{ $package->type }}</th>
                                </tr>
                                <tr>
                                    <th width="100px">Name</th>
                                    <th width="30px">:</th>
                                    <th>{{ $package->name }}</th>
                                </tr>
                                <tr>
                                    <th width="100px">Price</th>
                                    <th width="30px">:</th>
                                    <th>{{ $package->price }}</th>
                                </tr>
                                <tr>
                                    <th>
                                        <br>
                                        <a href="{{ url('/admin/package/index') }}"
                                            type="submit"class="btn btn-primary">Back</a>
                                        <br>
                                    </th>
                                </tr>
                            </table>
                        </div>
                    @endsection
