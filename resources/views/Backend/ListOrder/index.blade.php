@extends('layouts.backend_dashboard.app')
@section('Tampilan Crud')

@section('breadcrumbs')
    List Order
@endsection

@section('content')

    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title> List Order</title>
    </head>

    <body>
        <div class="container">
            <div class="container">
                <a href="{{ url('admin/listorder/formadd') }}" ttype="button" class="btn btn-warning">Add+</a><br>
                <div class="row g-3 align-items-center mb-3">
                    <div class="col-auto"></div>
                </div>
                <form method="GET" action="{{ url('admin/listorder/index') }}">
                    <input type="text" name="keyword" />
                    <button class="btn-xs-flat btn-success" type="submit">Search</button>
                    <button class="btn-xs-flat btn-success" type="submit" id="#filter">Filter</button>
                    <div id="filter">
                        <div class="modal-header">
                            <h4 class="modal-title">Filter</h4>
                        </div>
                        <div class="modal-body">
                            <div class="row">
                                <form autocomplete="off" method="get" action="{{ url('admin/dashboard/home') }}"
                                    enctype="multipart/form-data">
                                    <div class="col-md-3">
                                        <p style="font-size: 14px">Filter Code Order</p>
                                        <select class="form-control" name="type">
                                            <option value=""></option>
                                            <option value="Express" <?php echo g('type') == 'EX' ? ' selected="selected"' : ''; ?>>EX</option>
                                            <option value="Laundry" <?php echo g('type') == 'LD' ? ' selected="selected"' : ''; ?>>LD</option>
                                        </select>
                                        <br>
                                        <p style="font-size: 14px">Filter Status</p>
                                        <select class="form-control" name="status">
                                            <option value=""></option>
                                            <option value="Drop" <?php echo g('status') == 'Drop' ? ' selected="selected"' : ''; ?>>Drop</option>
                                            <option value="Take" <?php echo g('status') == 'Take' ? ' selected="selected"' : ''; ?>>Take</option>
                                            <option value="Finish" <?php echo g('status') == 'Finish' ? ' selected="selected"' : ''; ?>>Finish</option>
                                        </select>
                                    </div>
                                    <div class="col-md-3">

                                        <button type="submit" class="btn btn-warning" style="float: right;">Filter</button>

                                        <a class="btn btn-info" href="{{ url('admin/listorder/index') }}"
                                            style="float: right; margin-right: 4px;">Reset</a>
                                    </div>
                            </div>
                </form>
            </div>
        </div>
        </form>
        <br />
        <div class="row">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Code Order</th>
                        <th scope="col">Package Id</th>
                        <th scope="col">Total Price</th>
                        <th scope="col">User Name</th>
                        <th scope="col">User Phone</th>
                        <th scope="col">User Address</th>
                        <th scope="col">Status</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $id = 1; ?>
                    @foreach ($errors as $index => $data)
                        <tr>
                            <th scope="row">{{ $index + $errors->firstItem() }}</th>
                            <td>{{ $data->code_order }}</td>
                            <td>{{ $data->package_id }}</td>
                            <td>{{ $data->total_price }}</td>
                            <td>{{ $data->user_name }}</td>
                            <td>{{ $data->user_phone }}</td>
                            <td>{{ $data->user_address }}</td>
                            <td>{{ $data->status }}</td>
                            <td>
                                <a href="{{ url('/admin/listorder/detaillistorder/' . $data->id) }}"
                                    class="btn-sm btn-primary"> <i class="fas fa-folder"> </i> View</a>
                                <a href="{{ url('/admin/listorder/formedit/' . $data->id) }}" class="btn-sm btn-info"> <i
                                        class="fas fa-pencil-alt"> </i> Edit</a>
                                <a href="{{ url('/admin/listorder/delete/' . $data->id) }}" class="btn-sm btn-danger"> <i
                                        class="fas fa-trash"> </i> Delete</a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{ $errors->links() }}
        </div>
        </div>
        </div>


        <!-- Option 1: Bootstrap Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js"
            integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous">
        </script>

    </body>

    </html>

@endsection
