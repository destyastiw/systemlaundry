@extends('layouts.backend_dashboard.app')
@section('content')
    <!doctype html>
    <html lang="en">

    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- Bootstrap CSS -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet"
            integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <title>Detail List Order</title>
    </head>

    <body>
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">List Order Detail</h3>
                        </div>
                        <br />
                        <div class="container">
                            <div class="row justify-content-center">
                                <table class="card-body">
                                    <tr>
                                        <th width="120px">Code Order</th>
                                        <th width="30px">:</th>
                                        <th>{{ $listorder->code_order }}</th>
                                    </tr>
                                    <tr>
                                        <th width="120px">Package Id</th>
                                        <th width="30px">:</th>
                                        <th>{{ $listorder->package_id }}</th>
                                    </tr>
                                    <tr>
                                        <th width="120px">Total Price</th>
                                        <th width="30px">:</th>
                                        <th>{{ $listorder->total_price }}</th>
                                    </tr>
                                    <tr>
                                        <th width="120px">User Name</th>
                                        <th width="30px">:</th>
                                        <th>{{ $listorder->user_name }}</th>
                                    </tr>
                                    <tr>
                                        <th width="120px">User Phone</th>
                                        <th width="30px">:</th>
                                        <th>{{ $listorder->user_phone }}</th>
                                    </tr>
                                    <tr>
                                        <th width="120px">User Address</th>
                                        <th width="30px">:</th>
                                        <th>{{ $listorder->user_address }}</th>
                                    </tr>
                                    <tr>
                                        <th>
                                            <br>
                                            <a href="{{ url('/admin/listorder/index') }}"
                                                type="submit"class="btn btn-primary">Back</a>
                                            <br>
                                        </th>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    @endsection
