<?php

use App\Http\Controllers\Backend\Admin\AdminController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\AuthController;
use App\Http\Controllers\Backend\User\UserController;
use App\Http\Controllers\Backend\Package\PackageController;
use App\Http\Controllers\Backend\Listorder\ListorderController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/admin/auth/login', [AuthController::class, 'login'])->name('login');
Route::post('/admin/auth/postlogin', [AuthController::class, 'postLogin'])->name('postLogin');

Route::group(['middleware' => 'ceklevel'], function () {
    //DASHBOARD
    Route::get('/admin/dashboard/home', [DashboardController::class, 'getIndex']);
    // USER(ADMIN)
    Route::get('/admin/index', [AdminController::class, 'getIndex']);
    Route::get('/admin/formadd', [AdminController::class, 'getAdd']);
    Route::post('/admin/insertformadd', [AdminController::class, 'postSave']);
    Route::get('/admin/formedit/{id}', [AdminController::class, 'getEdit']);
    Route::post('/admin/updateformedit/{id}', [AdminController::class, 'postEdit']);
    Route::get('/admin/detailusers/{id}', [AdminController::class, 'getDetail']);
    Route::get('/admin/delete/{id}', [AdminController::class, 'getDelete']);
    // PACKAGE(PAKET)
    Route::get('/admin/package/index', [PackageController::class, 'getIndex']);
    Route::get('/admin/package/formadd', [PackageController::class, 'getAdd']);
    Route::post('/admin/package/insertformadd', [PackageController::class, 'postSave']);
    Route::get('/admin/package/formedit/{id}', [PackageController::class, 'getEdit']);
    Route::post('/admin/package/updateformedit/{id}', [PackageController::class, 'postEdit']);
    Route::get('/admin/package/detailpackage/{id}', [PackageController::class, 'getDetail']);
    Route::get('/admin/package/delete/{id}', [PackageController::class, 'getDelete']);
    // LIST ORDER
    Route::get('/admin/listorder/index', [ListOrderController::class, 'getIndex']);
    Route::get('/admin/listorder/formadd', [ListOrderController::class, 'getAdd']);
    Route::post('/admin/listorder/insertformadd', [ListOrderController::class, 'postSave']);
    Route::get('/admin/listorder/formedit/{id}', [ListOrderController::class, 'getEdit']);
    Route::post('/admin/listorder/updateformedit/{id}', [ListOrderController::class, 'postEdit']);
    Route::get('/admin/listorder/detaillistorder/{id}', [ListOrderController::class, 'getDetail']);
    Route::get('/admin/listorder/delete/{id}', [ListOrderController::class, 'getDelete']);
    // LAPORAN EXCEL
    Route::get('/admin/laporan/index', [ListorderController::class, 'index']);
    Route::get('/admin/laporan/export_excel', [ListorderController::class, 'export_excel']);
});


// FRONTEND
Route::get('/frontend', [App\Http\Controllers\FrontendController::class, 'index']);
Route::get('/frontend/order', [App\Http\Controllers\FrontendController::class, 'order']);

Route::get('/frontend/nota', [App\Http\Controllers\FrontendController::class, 'nota']);
Route::get('/frontend/printnota', [App\Http\Controllers\FrontendController::class, 'printnota']);

Route::get('/frontend/express', [App\Http\Controllers\FrontendController::class, 'express']);
Route::post('/frontend/express', [App\Http\Controllers\FrontendController::class, 'postSave']);
Route::get('/frontend/express/cari', [App\Http\Controllers\FrontendController::class, 'cari'])->name('cari');

Route::get('/frontend/laundry', [App\Http\Controllers\FrontendController::class, 'laundry']);
Route::post('/frontend/laundry', [App\Http\Controllers\FrontendController::class, 'postSave']);
Route::get('/frontend/laundry/cari', [App\Http\Controllers\FrontendController::class, 'mencari'])->name('mencari');

Route::get('/frontend/ambilpaket', [App\Http\Controllers\FrontendController::class, 'ambilpaket']);