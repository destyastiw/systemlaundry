<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderLaundrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('orders_laundry')->insert([
            'code_order' => 'EX1',
            'package_id' => '1',
            'total_price' => '20000',
            'user_name' => 'Huang Renjun',
            'user_phone' => '085267879054',
            'user_address' => 'Jilin,Tiongkok',
            'date_drop_laundry' => '2022-06-13',
            'date_take_laundry' => '2022-06-13',
            'date_finish_laundry' => '2022-06-14',
            'status' => 'finish',
            'created_at' => date("Y-m-d H-i-s"),
            'updated_at' => date("Y-m-d H-i-s"),
        ]);
    }
}