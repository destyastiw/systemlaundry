<?php

use Illuminate\Support\Facades\Request;

if (!function_exists('g')) {
    function g($name)
    {
        return Request::get($name);
    }
}